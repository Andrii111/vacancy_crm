from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Text
from datetime import datetime
from al_db import Base


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    email = Column(String(120), unique=True)
    login = Column(String(50), unique=True)
    password = Column(String(120), nullable=False)

    def __init__(self, name, email, login, password):
        self.name = name
        self.email = email
        self.login = login
        self.password = password

    def __repr__(self):
        return '<User %r' % self.name


class EmailCredentials(Base):
    __tablename__ = 'email_credentials'
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    email = Column(String(120), unique=True, nullable=False)
    login = Column(String(120), nullable=False)
    password = Column(String(120), nullable=False)
    pop_server = Column(String(50), nullable=True)
    smtp_server = Column(String(50), nullable=True)
    imap_server = Column(String(50), nullable=True)
    pop_port = Column(Integer, nullable=True)
    smtp_port = Column(Integer, nullable=True)
    imap_port = Column(Integer, nullable=True)

    def get_mandatory_fields(self):
        return {
            'login': self.login,
            'password': self.password,
            'user_email': self.email,
            'smtp_server': self.smtp_server,
            'smtp_port': self.smtp_port}

    def __init__(self, user_id, login, password, email, pop_server, smtp_server):
        self.user_id = user_id
        self.login = login
        self.password = password
        self.email = email
        self.pop_server = pop_server
        self.smtp_server = smtp_server

    def __repr__(self):
        return '<EmailCredentials %r>' % self.email


class Vacancy(Base):
    __tablename__ = 'vacancy'
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    creation_date = Column(DateTime, default=datetime.now)
    company = Column(String(120), nullable=False)
    contacts_ids = Column(String(120), nullable=False)
    description = Column(String(200), nullable=False)
    position_name = Column(String(120), nullable=False)
    comment = Column(String(120), nullable=True)
    status = Column(Integer, nullable=False)

    def __init__(self, company, contacts_ids, description, position_name, comment, status, user_id):
        self.company = company
        self.contacts_ids = contacts_ids
        self.description = description
        self.position_name = position_name
        self.comment = comment
        self.status = status
        self.user_id = user_id

    def __repr__(self):
        return '<Vacancy %r>' % self.position_name


class Event(Base):
    __tablename__ = 'event'
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    vacancy_id = Column(Integer, ForeignKey('vacancy.id'))
    description = Column(String(200), nullable=False)
    due_to_date = Column(String, nullable=False)
    title = Column(String(120), nullable=False)
    event_date = Column(DateTime, default=datetime.now)
    status = Column(Integer, nullable=False)

    def __init__(self, vacancy_id, description, title, due_to_date, status):
        self.vacancy_id = vacancy_id
        self.description = description
        self.title = title
        self.due_to_date = due_to_date

        self.status = status

    def __repr__(self):
        return f'event {self.title}'


class Template(Base):
    __tablename__ = 'template'
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    content = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey('user.id'))

    def __init__(self, name, content):
        self.name = name
        self.content = content

    def __repr__(self):
        return '<Template %r>' % self.name


class Document(Base):
    __tablename__ = 'document'
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    content = Column(Text, nullable=False)
    description = Column(String(200), nullable=False)
    user_id = Column(Integer, ForeignKey('user.id'))

    def __init__(self, name, content, description):
        self.name = name
        self.content = content
        self.description = description

    def __repr__(self):
        return '<Document %r>' % self.name
